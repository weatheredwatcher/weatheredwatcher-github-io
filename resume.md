---
layout: default
title: Resume
permalink: /resume/
---

David Duggins
===

![David Duggins](/assets/images/David.jpg "David Duggins")
_410 Castle Vale Rd Irmo SC 29063 USA_
[weatheredwatcher@gmail.com](mailto:weatheredwatcher@gmail.com) <br /> http:///weatheredwatcher.com


>With nearly 17 years experience in the IT world, I have made a focus on utilizing open source technologies to help business adopt inexpensive but effective IT strategies to grow their business. I strive to keep abreast of the ever changing world of IT and how FOSS effects it. I am an experienced software engineer and system developer who can bring a lot of start-up experience to any project.

Skills
====
Apache MySQL PHP jQuery JavaScript HTML 5 AJAX CSS CSS3 Responsive Design Git SQL Open Source CodeIgniter Ruby on Rails Microsoft SQL Server JSON Python Web Development Linux server administration ExpressionEngine Shell Scripting Linux System Administration C++ Java SaaS Ruby DevOps SOAP SOA XML Web Applications PaaS Cloud Applications Puppet WordPress MVC Bash REST Linux Web Services Zend Framework SASS Less Chef Lithium Node.js Grunt LAMP OS X

Experience
====
__OpenText10 / 2013 - Present__
_Consultant (Campaign Management)_
Working on the Campaign Strategy team. Support the development of campaigns for the various Business Units within Open Text. Manage the legacy servers and websites. 

* Red Hat/CentOS 6
* PHP
* WordPress
* Varnish
* Percona MySQL Cluster
* Load Balancing 
* Capistrano/Webistrano

__9Round Kickboxing Fitness 08 / 2014 - 2016__
_DevOps/Director of Programming_
Manage servers, security as well as manage the remote development team.  

* Agile
* Scrum
* PHP
* Git


__Alumnify 02/2014 - 07/2015__
_Technical Consultant_
Utilized my background in development, server side architecture and security to help guide Alumnify in creating robust, secure applications for the Alumni Engagement market. 

* PHP
* Linux
* Node JS
* Docker

__Freelance Developer 10/2011 - 05/2015__
_Systems Admin/Senior Developer_
I work as a DevOps consultant, managing distributed development teams, coding deployment scripts and other similar tasks. I work primarily with tools like Docker, Vagrant/Chef and git to manage the full software life cycle. I also manage ten + servers with loads ranging from a few 100 daily visitors to 100,000+ daily unique. I also freelance on small to mid size development projects. 

* php
* ruby
* python
* java/groovy/grails
* docker
* chef
* linux

__Comporium 01/2013 - 08/2013__
_PHP/Open Source Consultant_
Assisting in building out architecture and development standards by developing infrastructure, coding standards, best practices and providing training of development team. Working with developers, showing them Object Oriented PHP coding practices around the Codeigniter and Zend Frameworks. Assisting in the maintenance and further development of enterprise level php applications and infrastructure. Created and maintained project build and deploy scripts. Coded the front end, single-page architecture using various Javascript technologies such as Node JS, Backbone and Moustache. Evaluated alternative front end libraries such as Angular JS. 

* PHP
* MSSQL
* SOA
* SOAP/REST
* Codeigniter/Zend Server/Zend Framework
* LAMP Stack
* JavaScript(JQuery, Ember JS)
* Node JS
* Grunt/Bower
* Puppet
* Git/GitHub
* CI (Hudson, Unit Testing, Ant, Behat)
* CSS/Sass

__Pace Communications 09/2012 - 01/2013__
_Senior PHP Developer/Consultant_
Working on backend code for a custom content managment system based on WordPress. Using MySQL, PHP, JQuery, Ext3, Word Press, C#/ASP.net (converting old code into php), Responsive Design, html5, css3, Sass, Responsive Design, Twitter Bootstrap, SCRUM/Agile, json, xml, REST. Write and maintain scripts and utillities for data migration. Write and test plugins to render backend content and functionality into the WordPress system. Work with custom types, custom admin modules, customizingthe TinyMCE plugin and other parts of the backend editor.

__S.C. Budget and Control Board 05/2012 - 07/2012__
_php developer_
Built a custom system for managing contracts and work orders. 

* PHP 
* MySql 
* JavaScript

__Political Exchange 11/2010 - 2012__
_IT Consultant/Lead Developer_
Designed, built and tested the system architecture, manage designers and programmers, coordinate advertising efforts, administrate multiple servers (both production and development) 

* Gentoo Linux 
* Mac OSX Sever 
* PHP/CodeIgniter/Expression Engine

__TM Floyd 03/2011 - 08/2011__
_Consultant_
Worked on various contracts, using my knowledge of the web and my programming skills to make clients happy

__Rootloud 03/2010 - 10/2010__
_Director of IT/Lead Developer_
Created web-applications to assist in HR Training. Utilizing the latest in Open Source Technologies, as well as developing my own core system, I built systems that help companies train their employees. I maintained a Linux server in the cloud, and also developed custom applications in php and Ruby on Rails. 

* PHP/MVC 
* Ruby on Rails 
* MySQL 
* Linux 
* HTML5/CSS3/JavaScript

__Bonne Marque 07/2010 - 08/2010__
_Freelance Developer_
Preformed various tasks as a front end and back end developer. Joomla, Smarty, html, css, Javascript. Custom Application Coding.

__Leveraged Media 10 / 2009 - 02 / 2010__
_Director of IT/Lead Developer_
Directory of IT and lead programmer for Leveraged Media. I coded a custom built application written in php/mysql that provides a bridge between Netsuite and LM internal processes. I also work with JavaScript and Netsuite to provide extensible functionality for Leveraged Media. Built and maintained a local and a remote linux server for the application as well as the multi-site WordPress environment that we hosted.

__Midlands Technical College 08 / 2008 - 02 / 2009__
_Web-Developer_
Worked on various projects within the department of Instructional Design. Mainly involving Wordpress

__ISLC 03 / 2006 - 09 / 2008__
_IT/Web-Master_
Contracted with ISLC to create a new website presence and also to train staff in use of the web-site. Designed from scratch the new site, coded in php to allow easier modification for the staff.

* PHP
* Adobe Photoshop
* Javascript/ajax                                               
* Adobe Illustrator
* Adobe GoLive                                                  
* Apache
* Adobe Acrobat Professional                                    
* MySQL

__Self Employeed 03 / 2005 - 12 / 2005__
_Network Technician/Web-Design_
Sub-contracted for various companies and business doing IT work.

* Server 2000/2003
* Sonic Wall

* Exchange Server
* TrendMicro AntiVirus
* Windows 2000 Professional/XP professional                      
* CAT5 Installation

__Hunter Motors 09 / 2001 - 09 / 2004__
_Director of IT/Internet Sales_
Managed Internet Sales, worked on IT infrastructure including both wired and wireless networking. Assisted vendors in troubleshooting servers. Programmed business solutions and developed Sales Tools. Office, Access, VB, Red Hat, Bash Shell.

__The Great American Wheel Company 06 / 2003 - 08 / 2003__
_Consultant/Developer_
Work as a IT Consultant and Developer for a Custom Wheel Distributor. I set up there network and PC as well as built a e-commerce application in CFML from the ground up. I was nearly done with the code base when the company went bankrupt.

Education
====
__Midlands Technical College 2007 - 2011__
_AS-Information Technology (incomplete) , Information Technology_
Hanging with the other geeks and nerds playing magic, D&D and gaming on the school network.

__Forsyth Tecnical College 1999 - 2000__
_BS-CS_

__Wake Technical Community College 1999 - 1999__
_GED_

Interests
====
new technology, PHP, Linux, Free/Libre Software, 3d Printing, C/C++, Ajax, reading, history, politics, philosophy, writting

Languages
====
* English (Native Fluency)
* Russian  (Native Fluency)

Talks/Publications
====
__Codeigniter: From Zero to Blog in 15 minutes__
_Northeast PHP 2012 · Authors: David Duggins_
A talk that I gave at Northeast PHP conference in Boston, 2012

__Run Your Small Business on the Cheap With FOSS__
_POSSCON 2011 · Authors: David Duggins_
Spoke on using Open Source Technology to run Small/Mid-Sized Business and gave a workshop on using Linux and other FOSS programs

__What's New in PHP 5.40__
_POSSCON 2012 · Authors: David Duggins_
Hosted a BOF lunch on PHP. Talked about the new features of php 5.4 and gauged interest in a php user group in Columbia

__Cloud Development In The Cloud__
_Northeast PHP Conference 2013 · Authors: David Duggins_
Gave a talk on cloud based tools that one can use to develop web applications with.
