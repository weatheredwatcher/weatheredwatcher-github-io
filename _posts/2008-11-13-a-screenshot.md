---
title: a screenshot
slug: a-screenshot
date_published: 2008-11-13T05:00:00.000Z
date_updated: 2008-11-13T05:00:00.000Z
---

\n    Just thought that I would post a screenshot of my Desktop today.  Gentoo Linux with Songbird running.

![gentoo](http://www.weatheredwatcher.com/blog/wp-content/uploads/2008/11/thm_mydesktop.png)
