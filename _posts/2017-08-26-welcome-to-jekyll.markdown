---
layout: default
title:  "A New Blog and A Few Thoughts"
date:   2017-08-26
categories: hacking
tags: redhat, openshift, php, laravel
---
So, it's been a pretty long time since I have blogged.  Trying to fix that.  This is my new blog.  I will be migrating over most of my old posts here in the upcoming little bit...but after moving my blog so many fucking times, things have gotten crufty!.

So, I decided to give the OpenShift platform from RedHat another spin.  I started using it a few years ago when things were new.  But now they have something shiny and new, so I thought that I would give it a go.  To be fair, really not impresed.

For starters, it took over a month for them to provision an account for me.  I get that it's a free account, but goddamn...I can spin up a new container in Docker in mere seconds if I already have the images local.

So I finally got my own instance and so this evening I decided to throw something up.  After 15 minutes and my Laravel application is STILL not up and running.  This is not looking so good.  How am I suppose to make a valid judgement on the service if I am crippled **this** badly while I am testing it out?  I would **never** recommend this service to a client!

There are so many options out there for PAAS and they all work much faster then this seems to be working. It took a good 20 minutes for my application to finally deploy.
