---
title: Netbeans Beta (with SFTP Support)
slug: netbeans-beta-with-sftp-support
date_published: 2008-12-02T05:00:00.000Z
date_updated: 2008-12-02T05:00:00.000Z
---

\n    I downloaded the latest trunk release of Netbeans the other day.  I really wanted to test out the SFTP support.  I was not disappointed.  I loaded a project that I was working on at the moment.  I needed to upload the entire site one time.  This was a bit painful, but after the initial upload, it automatically uploaded the files that had changed.  It was great.  It took nothing more than a single button too!!
