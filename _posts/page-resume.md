---
title: Resume
slug: resume
date_published: 2015-12-21T06:52:05.091Z
date_updated: 2017-01-03T17:50:03.162Z
---

### David Duggins

*410 Castle Vale Rd Irmo SC 29063 USA*
[weatheredwatcher@gmail.com](mailto:weatheredwatcher@gmail.com)
[http:///weatheredwatcher.com](http:///weatheredwatcher.com)

> With nearly 17 years experience in the IT world, I have made a focus on utilizing open source technologies to help business adopt inexpensive but effective IT strategies to grow their business. I strive to keep abreast of the ever changing world of IT and how FOSS effects it. I am an experienced software engineer and system developer who can bring a lot of start-up experience to any project.

#### Skills

Apache MySQL PHP jQuery JavaScript HTML 5 AJAX CSS CSS3 Responsive Design Git SQL Open Source CodeIgniter Ruby on Rails Microsoft SQL Server JSON Python Web Development Linux server administration ExpressionEngine Shell Scripting Linux System Administration C++ Java SaaS Ruby DevOps SOAP SOA XML Web Applications PaaS Cloud Applications Puppet WordPress MVC Bash REST Linux Web Services Zend Framework SASS Less Chef Lithium Node.js Grunt LAMP OS X

#### Experience

**OpenText10 / 2013 - Present**
*Consultant (Campaign Management)*

Working on the Campaign Strategy team. Support the development of campaigns for the various Business Units within Open Text. Manage the legacy servers and websites. 

- Red Hat/CentOS 6
- PHP
- WordPress
- Varnish
- Percona MySQL Cluster
- Load Balancing 
- Capistrano/Webistrano

**9Round Kickboxing Fitness 08 / 2014 - 2016**
*DevOps/Director of Programming*

Manage servers, security as well as manage the remote development team.  

- Agile
- Scrum
- PHP
- Git

**Alumnify 02/2014 - 07/2015**
*Technical Consultant*

Utilized my background in development, server side architecture and security to help guide Alumnify in creating robust, secure applications for the Alumni Engagement market. 

- PHP
- Linux
- Node JS
- Docker

**Freelance Developer 10/2011 - 05/2015**
*Systems Admin/Senior Developer*

I work as a DevOps consultant, managing distributed development teams, coding deployment scripts and other similar tasks. I work primarily with tools like Docker, Vagrant/Chef and git to manage the full software life cycle. I also manage ten + servers with loads ranging from a few 100 daily visitors to 100,000+ daily unique. I also freelance on small to mid size development projects. 

- php
- ruby
- python
- java/groovy/grails
- docker
- chef
- linux

**Comporium 01/2013 - 08/2013**
*PHP/Open Source Consultant*

Assisting in building out architecture and development standards by developing infrastructure, coding standards, best practices and providing training of development team. Working with developers, showing them Object Oriented PHP coding practices around the Codeigniter and Zend Frameworks. Assisting in the maintenance and further development of enterprise level php applications and infrastructure. Created and maintained project build and deploy scripts. Coded the front end, single-page architecture using various Javascript technologies such as Node JS, Backbone and Moustache. Evaluated alternative front end libraries such as Angular JS. 

- PHP
- MSSQL
- SOA
- SOAP/REST
- Codeigniter/Zend Server/Zend Framework
- LAMP Stack
- JavaScript(JQuery, Ember JS)
- Node JS
- Grunt/Bower
- Puppet
- Git/GitHub
- CI (Hudson, Unit Testing, Ant, Behat)
- CSS/Sass

**Pace Communications 09/2012 - 01/2013**
*Senior PHP Developer/Consultant*

Working on backend code for a custom content managment system based on WordPress. Using MySQL, PHP, JQuery, Ext3, Word Press, C#/ASP.net (converting old code into php), Responsive Design, html5, css3, Sass, Responsive Design, Twitter Bootstrap, SCRUM/Agile, json, xml, REST. Write and maintain scripts and utillities for data migration. Write and test plugins to render backend content and functionality into the WordPress system. Work with custom types, custom admin modules, customizingthe TinyMCE plugin and other parts of the backend editor.

**S.C. Budget and Control Board 05/2012 - 07/2012**
*php developer*

Built a custom system for managing contracts and work orders. 

- PHP 
- MySql 
- JavaScript

**Political Exchange 11/2010 - 2012**
*IT Consultant/Lead Developer*

Designed, built and tested the system architecture, manage designers and programmers, coordinate advertising efforts, administrate multiple servers (both production and development) 

- Gentoo Linux 
- Mac OSX Sever 
- PHP/CodeIgniter/Expression Engine

**TM Floyd 03/2011 - 08/2011**
*Consultant*

Worked on various contracts, using my knowledge of the web and my programming skills to make clients happy

**Rootloud 03/2010 - 10/2010**
*Director of IT/Lead Developer*

Created web-applications to assist in HR Training. Utilizing the latest in Open Source Technologies, as well as developing my own core system, I built systems that help companies train their employees. I maintained a Linux server in the cloud, and also developed custom applications in php and Ruby on Rails. 

- PHP/MVC 
- Ruby on Rails 
- MySQL 
- Linux 
- HTML5/CSS3/JavaScript

**Bonne Marque 07/2010 - 08/2010**
*Freelance Developer*

Preformed various tasks as a front end and back end developer. Joomla, Smarty, html, css, Javascript. Custom Application Coding.

**Leveraged Media 10 / 2009 - 02 / 2010**
*Director of IT/Lead Developer*

Directory of IT and lead programmer for Leveraged Media. I coded a custom built application written in php/mysql that provides a bridge between Netsuite and LM internal processes. I also work with JavaScript and Netsuite to provide extensible functionality for Leveraged Media. Built and maintained a local and a remote linux server for the application as well as the multi-site WordPress environment that we hosted.

**Midlands Technical College 08 / 2008 - 02 / 2009**
*Web-Developer*

Worked on various projects within the department of Instructional Design. Mainly involving Wordpress

**ISLC 03 / 2006 - 09 / 2008**
*IT/Web-Master*

Contracted with ISLC to create a new website presence and also to train staff in use of the web-site. Designed from scratch the new site, coded in php to allow easier modification for the staff.

- PHP
- Adobe Photoshop
- Javascript/ajax                                               
- Adobe Illustrator
- Adobe GoLive                                                  
- Apache
- Adobe Acrobat Professional                                    
- MySQL

**Self Employeed 03 / 2005 - 12 / 2005**
*Network Technician/Web-Design*

Sub-contracted for various companies and business doing IT work.

- Server 2000/2003
- 
Sonic Wall

- 
Exchange Server

- TrendMicro AntiVirus
- Windows 2000 Professional/XP professional                      
- CAT5 Installation

**Hunter Motors 09 / 2001 - 09 / 2004**
*Director of IT/Internet Sales*

Managed Internet Sales, worked on IT infrastructure including both wired and wireless networking. Assisted vendors in troubleshooting servers. Programmed business solutions and developed Sales Tools. Office, Access, VB, Red Hat, Bash Shell.

**The Great American Wheel Company 06 / 2003 - 08 / 2003**
*Consultant/Developer*

Work as a IT Consultant and Developer for a Custom Wheel Distributor. I set up there network and PC as well as built a e-commerce application in CFML from the ground up. I was nearly done with the code base when the company went bankrupt.

#### Education

**Midlands Technical College 2007 - 2011**
*AS-Information Technology (incomplete) , Information Technology*

Hanging with the other geeks and nerds playing magic, D&D and gaming on the school network.

**Forsyth Tecnical College 1999 - 2000**
*BS-CS*

**Wake Technical Community College 1999 - 1999**
*GED*

#### Interests

new technology, PHP, Linux, Free/Libre Software, 3d Printing, C/C++, Ajax, reading, history, politics, philosophy, writting

#### Languages

- English (Native Fluency)
- Russian  (Native Fluency)

#### Talks/Publications

**Codeigniter: From Zero to Blog in 15 minutes**
*Northeast PHP 2012 · Authors: David Duggins*

A talk that I gave at Northeast PHP conference in Boston, 2012

**Run Your Small Business on the Cheap With FOSS**
*POSSCON 2011 · Authors: David Duggins*

Spoke on using Open Source Technology to run Small/Mid-Sized Business and gave a workshop on using Linux and other FOSS programs

**What's New in PHP 5.40**
*POSSCON 2012 · Authors: David Duggins*

Hosted a BOF lunch on PHP. Talked about the new features of php 5.4 and gauged interest in a php user group in Columbia

**Cloud Development In The Cloud**
*Northeast PHP Conference 2013 · Authors: David Duggins*

Gave a talk on cloud based tools that one can use to develop web applications with.
