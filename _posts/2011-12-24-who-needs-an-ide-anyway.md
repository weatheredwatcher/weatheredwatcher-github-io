---
title: Who Needs an IDE Anyway?
slug: who-needs-an-ide-anyway
date_published: 2011-12-24T05:00:00.000Z
date_updated: 2011-12-24T05:00:00.000Z
---

I've written a lot of posts about ide's over the years.  Since I am using codeigniter, I did a brief look around to see if there was a good ide for coding CI.  What I found was that a good MVC (like CI or Ruby) really negates the need for an ide.  A really good programmer's text editor works just fine.  I use TextMate.  
