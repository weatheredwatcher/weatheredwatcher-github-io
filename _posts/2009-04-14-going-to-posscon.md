---
title: Going to POSSCON!!
slug: going-to-posscon
date_published: 2009-04-14T04:00:00.000Z
date_updated: 2009-04-14T04:00:00.000Z
---

\n    Taking this next Saturday off and I am going to POSSCON, a local Open Source Conference here in Columbia...It will be good times!  I only wish there would be somthing on Ruby or PHP (as this would be of greater interest to me) but they will be speakers on Haskell and Erlang.  The keynote should be cool though.  [http://www.posscon.org](http://www.posscon.org)
